This sample app has been created by hamid.gholam@devolon.fi in order to show my familiarity with react.
In fact I used to develop front-end using Vue.js but as I realized that you have react.js in your stack, so I decided to do this assignment with react.js.

To run this application simply run this commands:
```angular2html
npm install
npm run start
```

This commands will run app in development and you can open it in browser with this address:
http://localhost:3000

Please make sure that port 3000 is open and free, otherwise it will run on 3001 port.

After running the app login the user credential you have made by postman register user request.
I have prepared a user for you to store in postman collection with following information:
email: hamid.gholami@devolon.fi
pass: 123456

Then simply you can store companies in any depth and store stations for them


Base api url is placed in ./services/api.js so in case of any change of backend change in port and address you can change it in the front as well.
Currently, base url for api calls is: http://0.0.0.0:8081/api/

Thanks for your time