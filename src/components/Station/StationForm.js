import React from "react";
import AuthService from "../../services/auth.service";
import api from "../../services/api";

export class StationForm extends React.Component
{
    constructor() {
        super();
        this.fetchCompanies = this.fetchCompanies.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    state = {
        currentUser: AuthService.getCurrentUser(),
        formData: {
            name: '',
            company_id: '',
            longitude: '',
            latitude: '',
            address: ''
        },
        submitting: false,
        companies: []
    }

    handleChange(event) {

        this.setState({
            ...this.state,
            formData: {
                ...this.state.formData,
                [event.target.name]: event.target.value
            },
        });

    }

    fetchCompanies() {
        api.get('company')
            .then(response => response.data)
            .then(res => {
                this.setState({...this.state, companies: res.data});
            });
    }

    componentDidMount() {
        this.fetchCompanies();
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            ...this.state,
            submitting: true
        });

        api.post('station', this.state.formData).then(response => {
            if (response.status === 200) {
                window.location = '/profile';
            }
        });
    }

    render() {
        return (
            <div className="container">
                <header className="jumbotron">
                    <h3>
                        <strong>Add new station</strong>
                    </h3>
                </header>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <fieldset>
                            <label>
                                <p>Name</p>
                                <input name="name" onChange={this.handleChange}/>
                            </label>
                        </fieldset>
                        <fieldset>
                            <label>
                                <p>Address</p>
                                <input name="address" onChange={this.handleChange}/>
                            </label>
                        </fieldset>
                        <fieldset>
                            <label>
                                <p>Longitude</p>
                                <input name="longitude" onChange={this.handleChange}/>
                            </label>
                        </fieldset>
                        <fieldset>
                            <label>
                                <p>Latitude</p>
                                <input name="latitude" onChange={this.handleChange}/>
                            </label>
                        </fieldset>
                        <fieldset>
                            <label>
                                <p>Company</p>
                                <select name="company_id" onChange={this.handleChange}>
                                    <option value=''>Select compnay</option>
                                    { this.state.companies.map((company,index) =>
                                        <option value={company.id} key={index}>{company.name}</option>)}
                                </select>
                            </label>
                        </fieldset>
                        <button type="submit">Submit</button>
                    </form>
                </div>
            </div>
        )
    }
}
