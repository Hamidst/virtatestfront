import React from "react";
import api from "../../services/api";
import { useLocation } from 'react-router-dom';

class StationsList extends React.Component
{
    constructor(props) {
        super(props);
    }

    state = {
        stations: [],
        childStations: [],
        company: []
    }

    fetchCompany(cid) {
        api.get('company/' + cid)
            .then(response => response)
            .then(res => {
                this.setState({
                    ...this.state,
                    company: res.data,
                    stations: res.data.stations,
                });

                let childStations = this.fetchChildrenStations(this.state.company);

                this.setState({
                    ...this.state,
                    childStations: childStations,
                });
            })
    }

    componentDidMount() {
        var url = new URL(window.location);
        var cid = url.searchParams.get("cid");

        if (cid === null) {
            window.location = '/profile';
        }

        this.fetchCompany(cid);
    }

    fetchChildrenStations(company) {
        let stations = [];

        if (company.stations !== undefined) {
            if (company.id !== this.state.company.id)
                stations = [...stations, ...company.stations]
                stations.forEach((station, index) => {
                    stations[index].companyName = company.name;
                })
        }

        if (company.children.length > 0) {
            company.children.forEach((item, key) => {
                stations = [...stations, ...this.fetchChildrenStations(item)]
            });
        }

        return stations;
    }

    render() {
        return (
            <div className="container">
                <header className="jumbotron">
                    <h3>
                        <strong>List stations for {this.state.company.name ? this.state.company.name : ' all companies'}</strong>
                    </h3>
                </header>
                <h3>Direct stations</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Longitude</th>
                        <th>Latitude</th>
                    </tr>
                    </thead>
                    <tbody>
                    { this.state.stations.map((station,index) =>
                        <tr key={index}>
                            <td>{station.id}</td>
                            <td>{station.name}</td>
                            <td>{station.longitude}</td>
                            <td>{station.latitude}</td>
                        </tr>
                    )}
                    </tbody>
                </table>
                <h3>Children stations</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Company</th>
                        <th>Longitude</th>
                        <th>Latitude</th>
                    </tr>
                    </thead>
                    <tbody>
                    { this.state.childStations.map((station,index) =>
                        <tr key={index}>
                            <td>{station.id}</td>
                            <td>{station.name}</td>
                            <td>{station.companyName}</td>
                            <td>{station.longitude}</td>
                            <td>{station.latitude}</td>
                        </tr>
                    )}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default StationsList;