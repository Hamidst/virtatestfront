import React from "react";
import AuthService from "../services/auth.service";
import CompanyList from "../components/Company/CompanyList";
import {Link} from "react-router-dom";

const Profile = () => {
  const currentUser = AuthService.getCurrentUser();

  return (
    <div className="container">
      <header className="jumbotron">
        <h3>
          <strong>{currentUser.user.name}</strong> Profile
        </h3>
        <strong>Email:</strong> {currentUser.user.email}
      </header>
        <Link to={"/company-form"} className="nav-link">
            add company
        </Link>
        <Link to={"/station-form"} className="nav-link">
            add Station
        </Link>

      <CompanyList></CompanyList>

    </div>
  );
};

export default Profile;
