import React from "react";
import AuthService from "../../services/auth.service";
import api from "../../services/api";

export class CompanyForm extends React.Component
{
    constructor(props) {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.fetchCompanies = this.fetchCompanies.bind(this);
    }

    state = {
        currentUser: AuthService.getCurrentUser(),
        formData: {
            name: '',
            parent_company_id: ''
        },
        submitting: false,
        companies: []
    }

    fetchCompanies() {
        api.get('company')
            .then(response => response.data)
            .then(res => {
                this.setState({...this.state, companies: res.data});
            })
    }

    componentDidMount() {
        this.fetchCompanies();
    }

    handleChange(event) {
        this.setState({
            ...this.state,
            formData: {
                ...this.state.formData,
                [event.target.name]: event.target.value
            },
        });

    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            ...this.state,
            submitting: true
        });

        api.post('company', this.state.formData).then(response => {
            if (response.status === 200) {
                window.location = '/profile';
            }
        });
    }

    render() {
        return (
            <div className="container">
                <header className="jumbotron">
                    <h3>
                        <strong>Add new company</strong>
                    </h3>
                </header>

                <div>
                    <form onSubmit={this.handleSubmit}>
                        <fieldset>
                            <label>
                                <p>Name</p>
                                <input name="name" onChange={this.handleChange}/>
                            </label>
                        </fieldset>
                        <fieldset>
                            <label>
                                <p>Parent</p>
                                <select name="parent_company_id" onChange={this.handleChange}>
                                    <option value=''>Select parent</option>
                                    { this.state.companies.map((company,index) =>
                                        <option value={company.id} key={index}>{company.name}</option>)}
                                </select>
                            </label>
                        </fieldset>
                        <button type="submit">Submit</button>
                    </form>
                </div>

            </div>
        )
    }
}