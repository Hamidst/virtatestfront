import React from "react";
import api from "../../services/api";
import {Link} from "react-router-dom";

class CompanyList extends React.Component {
    state = {
        companies: []
    }

    fetchCompanies() {
        api.get('company')
            .then(response => response.data)
            .then(res => {
                this.setState({...this.state, companies: res.data});
                this.setChildrenTotalValues();
            })
    }

    setChildrenTotalValues() {
        let newFormOfCompanies = this.state.companies;

        this.state.companies.forEach((company, index) => {
            newFormOfCompanies[index].totalChild = this.calculateChildrenTotalChild(company);
            newFormOfCompanies[index].totalStation = this.calculateChildrenTotalStation(company);
        })

        this.setState({...this.state, companies: newFormOfCompanies});
    }

    calculateChildrenTotalChild(company) {
        let totalSubCompany = company.children.length;
        if (company.children.length > 0) {
            company.children.forEach((item, key) => {
                totalSubCompany += this.calculateChildrenTotalChild(item);
            });
        }
        return totalSubCompany;
    }

    calculateChildrenTotalStation(company) {
        let totalStation = 0;
        if (company.stations !== undefined) {
            totalStation += company.stations.length;
        }

        if (company.children.length > 0) {
            company.children.forEach((item, key) => {
                totalStation += this.calculateChildrenTotalStation(item);
            });
        }

        return totalStation;
    }

    componentDidMount() {
        this.fetchCompanies()
    }

    render() {
        return (
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Parent</th>
                        <th>Total direct Stations</th>
                        <th>Total child company</th>
                        <th>Total station with children</th>
                        <th>Operations</th>
                    </tr>
                </thead>
                <tbody>
                { this.state.companies.map((company,index) =>
                    <tr key={index}>
                        <td>{company.id}</td>
                        <td>{company.name}</td>
                        <td>{company.parent_company ? company.parent_company.name: ''}</td>
                        <td>{company.stations? company.stations.length : 0}</td>
                        <td>{company.totalChild? company.totalChild : 0}</td>
                        <td>{company.totalStation? company.totalStation : 0}</td>
                        <td>
                            <Link to={"/stations?cid=" + company.id} className="nav-link">
                                Stations
                            </Link>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        )
    }
}

export default CompanyList;