import axios from 'axios';

const user = JSON.parse(localStorage.getItem('user'))

let token = '';
if (user !== null) {
    token = user.token;
}
export default axios.create({
    baseURL: `http://0.0.0.0:8081/api/`,
    headers: {
        'Authorization': `Bearer ${token}`
    }
});